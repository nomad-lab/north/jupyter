# JupyterLab - NOMAD Remote Tools Hub (NORTH)

We are use a custom image based on [jupyter/datascience-notebook](https://hub.docker.com/r/jupyter/datascience-notebook).
We add the latest version of the `nomad-lab` Python package. The image is build by the
CI/CD and stored in the projects container registry.

## To build the image

```
docker build -t gitlab-registry.mpcdf.mpg.de/nomad-lab/north/jupyter:latest .
```

