FROM jupyter/datascience-notebook:lab-3.6.2

# Fix: https://github.com/hadolint/hadolint/wiki/DL4006
# Fix: https://github.com/koalaman/shellcheck/wiki/SC3014
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

USER root

RUN apt update \
 && apt install --yes --quiet --no-install-recommends \
        libmagic-dev \
 && rm -rf /var/lib/apt/lists/*


# Switch back to jovyan to avoid accidental container runs as root
USER ${NB_UID}
WORKDIR "${HOME}"

RUN pip install --no-cache-dir \
    --pre --extra-index-url https://gitlab.mpcdf.mpg.de/api/v4/projects/2187/packages/pypi/simple \
        'nomad-lab'


# Get rid ot the following message when you open a terminal in jupyterlab:
# groups: cannot find name for group ID 11320
RUN touch ${HOME}/.hushlogin